package dk.mehmedbasic.goodenough;

import dk.mehmedbasic.goodenough.serialization.RootDefinition;
import dk.mehmedbasic.goodenough.serialization.VirtualDir;
import junit.framework.TestCase;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.util.List;

/**
 * TODO[jekm] - someone remind me to document this class. 

 * @author Jesenko Mehmedbasic
 * created 17-08-12, 21:20
 */
public class TestVirtualFileSystem extends TestCase {
   private VirtualDir root;
   private RootDefinition definition;

   public void setUp() throws Exception {
      super.setUp();
      Persister persister = new Persister();
      definition = persister.read(
            RootDefinition.class,
            getClass().getResourceAsStream("definition_small.xml"));
      root = definition.getRoot();

   }

   public void _testXmlParsing() throws Exception {
      assertEquals("The File Waiter", root.getName());
      assertEquals(4, root.getChildren().size());

      VirtualDir dir = root.getChildren().get(0);
      assertTrue(dir.isVirtual());

      assertEquals("movies", dir.getName());
      List<VirtualDir> sources = dir.getSources();
      assertEquals("d:/movies_dvd", sources.get(0).getPath());
      assertEquals("d:/movies_xvid", sources.get(1).getPath());
      assertEquals("d:/movies_hd", sources.get(2).getPath());

      assertFalse(sources.get(0).isVirtual());
      assertFalse(sources.get(1).isVirtual());
      assertFalse(sources.get(2).isVirtual());

      assertEquals(3, sources.size());

      VirtualDir test = root.getChildren().get(3);
      assertEquals("test of virtuals", test.getName());
      assertTrue(test.isVirtual());
      assertEquals(1, test.getChildren().size());

      test = test.getChildren().get(0);
      assertEquals(1, test.getChildren().size());
      assertEquals("a virtual sub-directory (src)", test.getChildren().get(0).getName());
   }

   public void testDirectoryResolving() throws Exception {
      DefinitionReader reader = new DefinitionReader();
      AbstractFile resolve = reader.read(definition);
      List<AbstractFile> list = resolve.listFiles();
      assertEquals("List size should be 1,", 1, list.size());
   }

   private List<String> resolveFile(VirtualDir virtualDir, List<String> filesInRoot, int indent) {

      String path = virtualDir.getPath();
      findFilesFromPath(path, filesInRoot, indent);

      List<VirtualDir> sources = virtualDir.getSources();
      for (VirtualDir source : sources) {
         resolveFile(source, filesInRoot, indent);
      }
      for (VirtualDir source : virtualDir.getChildren()) {
         filesInRoot.add(getIndentation(indent) + source.getName());
         resolveFile(source, filesInRoot, indent + 1);
      }
      return filesInRoot;
   }

   private void findFilesFromPath(String path, List<String> filesInRoot, int indent) {
      if (path != null) {
         File file = new File(path);
         String[] list = file.list();
         for (String s : list) {
            filesInRoot.add(getIndentation(indent) + s);
         }
      }
   }

   private String getIndentation(int indent) {
      String foo = "";
      for (int i = 0; i < indent; i++) {
         foo += "\t";
      }
      return foo;
   }

   @Override
   protected void tearDown() throws Exception {
      super.tearDown();
      root = null;
      definition = null;
   }
}
