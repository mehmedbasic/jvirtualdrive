package dk.mehmedbasic.goodenough;

import dk.mehmedbasic.goodenough.serialization.RootDefinition;
import org.simpleframework.xml.core.Persister;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * A bunch of constants/helper methods for said constants.

 * @author Jesenko Mehmedbasic
 * created 05-07-12, 01:14
 */
public class Constants {
   public static final String SOFTWARE_NAME = "JVirtualDrive";
   public static final String SOFTWARE_VERSION = "1.0a";
   public static int PORT = 1337;

   public static String CONFIG_ROOT = System.getProperty("user.home") + "/.goodenoughserver";
   public static String SERVER_NAME = "";

   private static final int TIMEOUT_MINUTES = 10;
   private static final int TIMEOUT_SECONDS = TIMEOUT_MINUTES * 60;
   public static final int TIMEOUT = TIMEOUT_SECONDS * 1000;

   private static String cachedHostLookup = null;

   public static AbstractFile resolveRoot() throws Exception {
      DefinitionReader reader = new DefinitionReader();
      InputStream resourceAsStream = new FileInputStream(CONFIG_ROOT + "/definition.xml");
      RootDefinition read = new Persister().read(RootDefinition.class, resourceAsStream);
      SERVER_NAME = read.getServer();
      ensureName();
      return reader.read(read);
   }

   private static void ensureName() {
      if (SERVER_NAME == null || SERVER_NAME.trim().isEmpty()) {
         if (cachedHostLookup == null) {
            cachedHostLookup = guessName();
         }
         SERVER_NAME = cachedHostLookup;
      }
   }

   private static String version() {
      return Constants.SOFTWARE_NAME + " v" + Constants.SOFTWARE_VERSION;
   }

   public static String versionString() {
      return Constants.version() + " @ " + Constants.SERVER_NAME + " Port " + Constants.PORT;
   }

   private static String guessName() {
      try {
         InetAddress address = InetAddress.getLocalHost();
         return address.getHostName();
      } catch (UnknownHostException ignored) {
      }
      return "UnknownServer";
   }
}
