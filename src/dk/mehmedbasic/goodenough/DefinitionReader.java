package dk.mehmedbasic.goodenough;

import dk.mehmedbasic.goodenough.serialization.RootDefinition;
import dk.mehmedbasic.goodenough.serialization.VirtualDir;

import java.io.File;

/**
 * A class that reads the path definition for the server.

 * @author Jesenko Mehmedbasic
 * created 17-08-12, 22:15
 */
public class DefinitionReader {

   public AbstractFile read(RootDefinition definition) {
      VirtualDir definitionRoot = definition.getRoot();
      VirtualFile resolve = (VirtualFile) read(new VirtualFile(""), definitionRoot);
      resolve.setDirectory(true);
      PathResolver.registerFile("/", resolve);

      return resolve;
   }

   private AbstractFile read(VirtualFile root, VirtualDir definition) {

      String name = definition.getName();
      if (name == null) {
         name = "";
      }

      if (definition.isVirtual()) {
         VirtualFile imaginaryRoot = new VirtualFile(root.getRoot() + name);
         imaginaryRoot.setName(name);
         imaginaryRoot.setParent(root);
         imaginaryRoot.setDirectory(true);
         for (VirtualDir source : definition.getSources()) {
            AbstractFile file = read(imaginaryRoot, source);
            imaginaryRoot.getSources().add(file);
         }

         for (VirtualDir source : definition.getChildren()) {
            AbstractFile file = read(imaginaryRoot, source);
            imaginaryRoot.addChild(file);
         }
         return imaginaryRoot;
      } else {
         return new JavaFile(new File(definition.getPath()));
      }
   }
}
