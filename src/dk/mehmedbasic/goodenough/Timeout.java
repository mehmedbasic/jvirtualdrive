package dk.mehmedbasic.goodenough;

import org.apache.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * A timeout no-op helper.
 *
 * Ensures the server can timeout the reloading of definitions when requests are not being received.
 *
 * @author Jesenko Mehmedbasic
 * created 10/1/13, 2:46 AM
 */
public class Timeout {
   private static long lastAccessed = System.currentTimeMillis();
   private static final Logger logger = Logger.getLogger(Timeout.class);
   private static final ThreadLocal<Timer> currentTimer = new ThreadLocal<Timer>();

   private static synchronized void serverCalled() {
      lastAccessed = System.currentTimeMillis();
      logger.debug("Last accessed updated to " + lastAccessed);
   }

   public static synchronized long timeSinceLastRequest() {
      return System.currentTimeMillis() - lastAccessed;
   }

   public static void startTimer() {
      if (currentTimer.get() != null) {
         throw new RuntimeException("Illegal state, thread local timer was not null");
      }
      logger.debug("Timer started");
      Timer timer = new Timer();
      timer.schedule(new TimeoutUpdateTask(), 0, 10 * 1000);
      currentTimer.set(timer);
   }

   public static void stopTimer() {
      if (currentTimer.get() != null) {
         logger.debug("Timer stopped");
         currentTimer.get().cancel();
         currentTimer.set(null);
      }
   }

   private static final class TimeoutUpdateTask extends TimerTask {
      @Override
      public void run() {
         serverCalled();
      }
   }
}
