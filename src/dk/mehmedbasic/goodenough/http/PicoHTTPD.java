package dk.mehmedbasic.goodenough.http;

import dk.mehmedbasic.goodenough.*;
import dk.mehmedbasic.goodenough.directorylistings.HtmlRenderer;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * A very simple HTTP
 */
public class PicoHTTPD {
   private static Logger logger = Logger.getLogger(PicoHTTPD.class);

   private static final String SERVER_NAME = "PicoHTTPD";
   private static final int BUFFER_SIZE = 16 * 1024;


   static {
      try {
         MemoryCache.init();
      } catch (Exception e) {
         throw new Error("Failure creating MemoryCache.");
      }
   }

   /**
    * Mapping (String)FILENAME_EXTENSION -> (String)MIME_TYPE
    */
   private static Map<String, String> theMimeTypes = new HashMap<String, String>();

   private static final Map<String, HtmlRenderer> htmlRendererMap = new HashMap<String, HtmlRenderer>(10, 1);

   /**
    * Starts a HTTP server to given port.<p>
    * Throws an IOException if the socket is already in use
    */
   public PicoHTTPD(AbstractFile serverRoot) throws IOException {
      this.myRootDir = serverRoot;

      serverSocket = new ServerSocket(Constants.PORT);
      connectionAcceptThread = new Thread(
            new Runnable() {
               public void run() {
                  try {
                     Socket socket;
                     while ((socket = serverSocket.accept()) != null) {
                        HTTPSession session = new HTTPSession(socket);
                        Thread t = new Thread(session);
                        t.setDaemon(true);
                        t.start();
                     }
                  } catch (IOException e) {
                     logger.error("Error while connecting to client", e);
                  }
               }
            });
      connectionAcceptThread.setDaemon(true);
      connectionAcceptThread.start();
      logger.info("Running " + Constants.versionString());
      logger.info("Using definition file: " + serverRoot.getName());
   }

   public static List<String> getPathFromUri(String uri) {
      uri = uri.replaceAll("//", "/");
      ArrayList<String> result = new ArrayList<String>();
      if (uri.startsWith("/")) {
         result.add("/");
      }
      if (uri.length() > 1) {
         for (String token : uri.split("/")) {
            if (token.trim().length() > 0) {
               result.add(token);
            }
         }
      }
      return result;
   }

   /**
    * HTTP response.
    * Return one of these from serve().
    */
   static final class Response {


      /**
       * HTTP status code after processing, e.g. "200 OK", HTTP_OK
       */
      public String status;

      /**
       * MIME type of content, e.g. "text/html"
       */
      public String mimeType;

      /**
       * Data of the response, may be null.
       */
      public InputStream data;
      /**
       * Headers for the HTTP response. Use addHeader()
       * to add lines.
       */
      public Properties header = new Properties();

      /**
       * Default constructor: response = HTTP_OK, data = mime = 'null'
       */
      public Response() {
         this.status = HTTP_OK;
      }

      /**
       * Adds given line to the header.
       */
      public void addHeader(String name, String value) {
         header.put(name, value);
      }

      /**
       * Basic constructor.
       */
      public Response(String status, String mimeType, InputStream data) {
         this.status = status;
         this.mimeType = mimeType;
         this.data = data;
      }

      /**
       * Convenience method that makes an InputStream out of
       * given text.
       */
      public Response(String status, String mimeType, String txt) {
         this.status = status;
         this.mimeType = mimeType;
         try {
            this.data = new ByteArrayInputStream(txt.getBytes("UTF-8"));
         } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
         }
      }


   }

   /**
    * Some HTTP response status codes
    */
   public static final String
         HTTP_OK = "200 OK",
         HTTP_PARTIALCONTENT = "206 Partial Content",
         HTTP_RANGE_NOT_SATISFIABLE = "416 Requested Range Not Satisfiable",
         HTTP_REDIRECT = "301 Moved Permanently",
         HTTP_NOTMODIFIED = "304 Not Modified",
         HTTP_FORBIDDEN = "403 Forbidden",
         HTTP_NOTFOUND = "404 Not Found",
         HTTP_BADREQUEST = "400 Bad Request",
         HTTP_INTERNALERROR = "500 Internal Server Error",
         HTTP_NOTIMPLEMENTED = "501 Not Implemented";

   /**
    * Common mime types for dynamic content
    */
   public static final String
         MIME_PLAINTEXT = "text/plain",
         MIME_HTML = "text/html",
         MIME_DEFAULT_BINARY = "application/octet-stream",
         MIME_XML = "text/xml";

   /**
    * Override this to customize the server.<p>
    *
    * (By default, this delegates to serveFile() and allows directory listing.)
    *
    *
    * @param uri         Percent-decoded URI without parameters, for example "/index.cgi"
    * @param method      "GET", "POST" etc.
    * @param header      Header entries, percent decoded
    * @param parms       Parsed, percent decoded parameters from URI and, in case of POST, data.
    * @param hostAddress host address
    * @return HTTP response, see class Response for details
    */
   public Response serve(
         String uri,
         String method,
         Properties header,
         Properties parms,
         Properties files, String hostAddress) throws IOException {
      logger.info(method + " '" + uri + "' from " + hostAddress);

      Enumeration e = header.propertyNames();
      while (e.hasMoreElements()) {
         String value = (String) e.nextElement();
         logger.debug("  HDR: '" + value + "' = '" + header.getProperty(value) + "'");
      }
      e = parms.propertyNames();
      while (e.hasMoreElements()) {
         String value = (String) e.nextElement();
         logger.debug("  PRM: '" + value + "' = '" + parms.getProperty(value) + "'");
      }
      e = files.propertyNames();
      while (e.hasMoreElements()) {
         String value = (String) e.nextElement();
         logger.debug("  UPLOADED: '" + value + "' = '" + files.getProperty(value) + "'");
      }

      return serveFile(uri, header, myRootDir, true);
   }

   /**
    * Stops the server.
    */
   public void stop() {
      try {
         serverSocket.close();
         connectionAcceptThread.join();
      } catch (Exception e) {
         logger.error("Error while shutting down server", e);
      }
   }

   /**
    * Handles one session, i.e. parses the HTTP request
    * and returns the response.
    */
   private class HTTPSession implements Runnable {
      private SimpleDateFormat dateFormat;
      String hostAddress;

      private Socket socket;
      private Timer timer;

      public HTTPSession(Socket s) {
         socket = s;
         dateFormat = createGmtDateFormat();

         InetSocketAddress remoteSocketAddress = (InetSocketAddress) socket.getRemoteSocketAddress();
         hostAddress = remoteSocketAddress.getAddress().getHostAddress();
      }

      public void run() {
         try {
            Timeout.startTimer();

            InputStream is = socket.getInputStream();
            if (is == null) {
               return;
            }

            // Read the first 8192 bytes.
            // The full header should fit in here.
            // Apache's default header limit is 8KB.
            int bufferSize = 8192;
            byte[] buf = new byte[bufferSize];
            int rlen = is.read(buf, 0, bufferSize);
            if (rlen <= 0) {
               return;
            }

            // Create a BufferedReader for parsing the header.
            ByteArrayInputStream hbis = new ByteArrayInputStream(buf, 0, rlen);
            BufferedReader hin = new BufferedReader(new InputStreamReader(hbis));
            Properties pre = new Properties();
            Properties parms = new Properties();
            Properties header = new Properties();
            Properties files = new Properties();

            // Decode the header into parms and header java properties
            decodeHeader(hin, pre, parms, header);
            String method = pre.getProperty("method");
            String uri = pre.getProperty("uri");

            long size = 0x7FFFFFFFFFFFFFFFl;
            String contentLength = header.getProperty("content-length");
            if (contentLength != null) {
               try {
                  size = Integer.parseInt(contentLength);
               } catch (NumberFormatException ignored) {
               }
            }

            // We are looking for the byte separating header from body.
            // It must be the last byte of the first two sequential new lines.
            int splitbyte = 0;
            boolean sbfound = false;
            while (splitbyte < rlen) {
               if (buf[splitbyte] == '\r' && buf[++splitbyte] == '\n' && buf[++splitbyte] == '\r' && buf[++splitbyte] == '\n') {
                  sbfound = true;
                  break;
               }
               splitbyte++;
            }
            splitbyte++;

            // Write the part of body already read to ByteArrayOutputStream f
            ByteArrayOutputStream f = new ByteArrayOutputStream();
            if (splitbyte < rlen) {
               f.write(buf, splitbyte, rlen - splitbyte);
            }

            // While Firefox sends on the first read all the data fitting
            // our buffer, Chrome and Opera sends only the headers even if
            // there is data for the body. So we do some magic here to find
            // out whether we have already consumed part of body, if we
            // have reached the end of the data to be sent or we should
            // expect the first byte of the body at the next read.
            if (splitbyte < rlen) {
               size -= rlen - splitbyte + 1;
            } else if (!sbfound || size == 0x7FFFFFFFFFFFFFFFl) {
               size = 0;
            }

            // Now read all the body and write it to f
            buf = new byte[512];
            while (rlen >= 0 && size > 0) {
               rlen = is.read(buf, 0, 512);
               size -= rlen;
               if (rlen > 0) {
                  f.write(buf, 0, rlen);
               }
            }

            // Get the raw body as a byte []
            byte[] fbuf = f.toByteArray();

            // Create a BufferedReader for easily reading it as string.
            ByteArrayInputStream bin = new ByteArrayInputStream(fbuf);
            BufferedReader in = new BufferedReader(new InputStreamReader(bin, "UTF-8"));

            // If the method is POST, there may be parameters
            // in data section, too, read it:
            if (method.equalsIgnoreCase("POST")) {
               String contentType = "";
               String contentTypeHeader = header.getProperty("content-type");
               StringTokenizer st = new StringTokenizer(contentTypeHeader, "; ");
               if (st.hasMoreTokens()) {
                  contentType = st.nextToken();
               }

               if (contentType.equalsIgnoreCase("multipart/form-data")) {
                  // Handle multipart/form-data
                  if (!st.hasMoreTokens()) {
                     sendError(
                           HTTP_BADREQUEST,
                           "BAD REQUEST: Content type is multipart/form-data but boundary missing. Usage: GET /example/file.html");
                  }
                  String boundaryExp = st.nextToken();
                  st = new StringTokenizer(boundaryExp, "=");
                  if (st.countTokens() != 2) {
                     sendError(
                           HTTP_BADREQUEST,
                           "BAD REQUEST: Content type is multipart/form-data but boundary syntax error. Usage: GET /example/file.html");
                  }
                  st.nextToken();
                  String boundary = st.nextToken();

                  decodeMultipartData(boundary, fbuf, in, parms, files);
               } else {
                  // Handle application/x-www-form-urlencoded
                  String postLine = "";
                  char pbuf[] = new char[512];
                  int read = in.read(pbuf);
                  while (read >= 0 && !postLine.endsWith("\r\n")) {
                     postLine += String.valueOf(pbuf, 0, read);
                     read = in.read(pbuf);
                  }
                  postLine = postLine.trim();
                  decodeParms(postLine, parms);
               }
            }

            if (method.equalsIgnoreCase("PUT")) {
               files.put("content", saveTmpFile(fbuf, 0, f.size()));
            }

            // Ok, now do the serve()
            Response response = serve(uri, method, header, parms, files, hostAddress);
            if (response == null) {
               sendError(HTTP_INTERNALERROR, "SERVER INTERNAL ERROR: Serve() returned a null response.");
            } else {
               sendResponse(response.status, response.mimeType, response.header, response.data);
            }

            in.close();
            is.close();
         } catch (IOException ioe) {
            try {
               sendError(HTTP_INTERNALERROR, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
            } catch (Throwable t) {
               logger.error("Error while sending error to client", t);
            }
         } catch (Throwable ignored) {
            // Thrown by sendError, ignore and exit the thread.
         }

         Timeout.stopTimer();
      }

      /**
       * Decodes the sent headers and loads the data into
       * java Properties' key - value pairs
       **/
      private void decodeHeader(BufferedReader in, Properties pre, Properties parms, Properties header)
            throws InterruptedException {
         try {
            // Read the request line
            String inLine = in.readLine();
            if (inLine == null) {
               return;
            }
            StringTokenizer st = new StringTokenizer(inLine);
            if (!st.hasMoreTokens()) {
               sendError(HTTP_BADREQUEST, "BAD REQUEST: Syntax error. Usage: GET /example/file.html");
            }

            String method = st.nextToken();
            pre.put("method", method);

            if (!st.hasMoreTokens()) {
               sendError(HTTP_BADREQUEST, "BAD REQUEST: Missing URI. Usage: GET /example/file.html");
            }

            String uri = st.nextToken();

            // Decode parameters from the URI
            int qmi = uri.indexOf('?');
            if (qmi >= 0) {
               decodeParms(uri.substring(qmi + 1), parms);
               uri = decodePercent(uri.substring(0, qmi));
            } else {
               uri = decodePercent(uri);
            }

            // If there's another token, it's protocol version,
            // followed by HTTP headers. Ignore version but parse headers.
            // NOTE: this now forces header names lowercase since they are
            // case insensitive and vary by client.
            if (st.hasMoreTokens()) {
               String line = in.readLine();
               while (line != null && line.trim().length() > 0) {
                  int p = line.indexOf(':');
                  if (p >= 0) {
                     header.put(line.substring(0, p).trim().toLowerCase(), line.substring(p + 1).trim());
                  }
                  line = in.readLine();
               }
            }

            pre.put("uri", uri);
         } catch (IOException ioe) {
            sendError(HTTP_INTERNALERROR, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
         }
      }

      /**
       * Decodes the Multipart Body data and put it
       * into java Properties' key - value pairs.
       **/
      private void decodeMultipartData(
            String boundary,
            byte[] fbuf,
            BufferedReader in,
            Properties parms,
            Properties files)
            throws InterruptedException {
         try {
            int[] bpositions = getBoundaryPositions(fbuf, boundary.getBytes());
            int boundarycount = 1;
            String mpline = in.readLine();
            while (mpline != null) {
               if (!mpline.contains(boundary)) {
                  sendError(
                        HTTP_BADREQUEST,
                        "BAD REQUEST: Content type is multipart/form-data but next chunk does not start with boundary. Usage: GET /example/file.html");
               }
               boundarycount++;
               Properties item = new Properties();
               mpline = in.readLine();
               while (mpline != null && mpline.trim().length() > 0) {
                  int p = mpline.indexOf(':');
                  if (p != -1) {
                     item.put(mpline.substring(0, p).trim().toLowerCase(), mpline.substring(p + 1).trim());
                  }
                  mpline = in.readLine();
               }
               if (mpline != null) {
                  String contentDisposition = item.getProperty("content-disposition");
                  if (contentDisposition == null) {
                     sendError(
                           HTTP_BADREQUEST,
                           "BAD REQUEST: Content type is multipart/form-data but no content-disposition info found. Usage: GET /example/file.html");
                  }
                  StringTokenizer st = new StringTokenizer(contentDisposition, "; ");
                  Properties disposition = new Properties();
                  while (st.hasMoreTokens()) {
                     String token = st.nextToken();
                     int p = token.indexOf('=');
                     if (p != -1) {
                        disposition.put(token.substring(0, p).trim().toLowerCase(), token.substring(p + 1).trim());
                     }
                  }
                  String pname = disposition.getProperty("name");
                  pname = pname.substring(1, pname.length() - 1);

                  String value = "";
                  if (item.getProperty("content-type") == null) {
                     while (mpline != null && !mpline.contains(boundary)) {
                        mpline = in.readLine();
                        if (mpline != null) {
                           int d = mpline.indexOf(boundary);
                           if (d == -1) {
                              value += mpline;
                           } else {
                              value += mpline.substring(0, d - 2);
                           }
                        }
                     }
                  } else {
                     if (boundarycount > bpositions.length) {
                        sendError(HTTP_INTERNALERROR, "Error processing request");
                     }
                     int offset = stripMultipartHeaders(fbuf, bpositions[boundarycount - 2]);
                     String path = saveTmpFile(fbuf, offset, bpositions[boundarycount - 1] - offset - 4);
                     files.put(pname, path);
                     value = disposition.getProperty("filename");
                     value = value.substring(1, value.length() - 1);
                     do {
                        mpline = in.readLine();
                     } while (mpline != null && !mpline.contains(boundary));
                  }
                  parms.put(pname, value);
               }
            }
         } catch (IOException ioe) {
            sendError(HTTP_INTERNALERROR, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
         }
      }

      /**
       * Find the byte positions where multipart boundaries start.
       **/
      public int[] getBoundaryPositions(byte[] b, byte[] boundary) {
         int matchcount = 0;
         int matchbyte = -1;
         Vector<Integer> matchbytes = new Vector<Integer>();
         for (int i = 0; i < b.length; i++) {
            if (b[i] == boundary[matchcount]) {
               if (matchcount == 0) {
                  matchbyte = i;
               }
               matchcount++;
               if (matchcount == boundary.length) {
                  matchbytes.addElement(matchbyte);
                  matchcount = 0;
                  matchbyte = -1;
               }
            } else {
               i -= matchcount;
               matchcount = 0;
               matchbyte = -1;
            }
         }
         int[] ret = new int[matchbytes.size()];
         for (int i = 0; i < ret.length; i++) {
            ret[i] = matchbytes.elementAt(i).intValue();
         }
         return ret;
      }


      /**
       * Retrieves the content of a sent file and saves it
       * to a temporary file.
       * The full path to the saved file is returned.
       **/
      private String saveTmpFile(byte[] b, int offset, int len) {
         String path = "";
         if (len > 0) {
            String tmpdir = System.getProperty("java.io.tmpdir");
            try {
               File temp = File.createTempFile(SERVER_NAME, "", new File(tmpdir));
               OutputStream fstream = new FileOutputStream(temp);
               fstream.write(b, offset, len);
               fstream.close();
               path = temp.getAbsolutePath();
            } catch (Exception e) { // Catch exception if any
               logger.error("An error occurred", e);
            }
         }
         return path;
      }

      /**
       * It returns the offset separating multipart file headers
       * from the file's data.
       **/
      private int stripMultipartHeaders(byte[] b, int offset) {
         int i;
         for (i = offset; i < b.length; i++) {
            if (b[i] == '\r' && b[++i] == '\n' && b[++i] == '\r' && b[++i] == '\n') {
               break;
            }
         }
         return i + 1;
      }

      /**
       * Decodes the percent encoding scheme. <br/>
       * For example: "an+example%20string" -> "an example string"
       */
      private String decodePercent(String str) throws InterruptedException {
         try {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
               char c = str.charAt(i);
               switch (c) {
                  case '+':
                     builder.append(' ');
                     break;
                  case '%':
                     builder.append((char) Integer.parseInt(str.substring(i + 1, i + 3), 16));
                     i += 2;
                     break;
                  default:
                     builder.append(c);
                     break;
               }
            }
            return builder.toString();
         } catch (Exception e) {
            sendError(HTTP_BADREQUEST, "BAD REQUEST: Bad percent-encoding.");
            return null;
         }
      }

      /**
       * Decodes parameters in percent-encoded URI-format
       * ( e.g. "name=Jack%20Daniels&pass=Single%20Malt" ) and
       * adds them to given Properties. NOTE: this doesn't support multiple
       * identical keys due to the simplicity of Properties -- if you need multiples,
       * you might want to replace the Properties with a Hashtable of Vectors or such.
       */
      private void decodeParms(String parms, Properties p)
            throws InterruptedException, UnsupportedEncodingException {
         if (parms == null) {
            return;
         }

         StringTokenizer st = new StringTokenizer(parms, "&");
         while (st.hasMoreTokens()) {
            String e = st.nextToken();
            int sep = e.indexOf('=');
            if (sep >= 0) {
               p.put(
                     decodePercent(e.substring(0, sep)).trim(),
                     URLDecoder.decode(e.substring(sep + 1), "UTF-8"));
            }
         }
      }

      /**
       * Returns an error message as a HTTP response and
       * throws InterruptedException to stop further request processing.
       */
      private void sendError(String status, String msg) throws InterruptedException {
         sendResponse(status, MIME_PLAINTEXT, null, new ByteArrayInputStream(msg.getBytes()));
         throw new InterruptedException();
      }

      /**
       * Sends given response to the socket.
       */
      private void sendResponse(String status, String mime, Properties header, InputStream data) {
         try {
            if (status == null) {
               throw new Error("sendResponse(): Status can't be null.");
            }

            OutputStream out = socket.getOutputStream();
            PrintWriter pw = new PrintWriter(out);
            pw.print("HTTP/1.0 " + status + " \r\n");

            if (mime != null) {
               pw.print("Content-Type: " + mime + "\r\n");
            }

            if (header == null || header.getProperty("Date") == null) {
               pw.print("Date: " + dateFormat.format(new Date()) + "\r\n");
            }

            if (header != null) {
               Enumeration e = header.keys();
               while (e.hasMoreElements()) {
                  String key = (String) e.nextElement();
                  String value = header.getProperty(key);
                  pw.print(key + ": " + value + "\r\n");
               }
            }

            pw.print("\r\n");
            pw.flush();

            if (data != null) {
               int pending = data.available();   // This is to support partial sends, see serveFile()
               byte[] buff = new byte[BUFFER_SIZE];
               while (pending > 0) {
                  int read = data.read(buff, 0, ((pending > BUFFER_SIZE) ? BUFFER_SIZE : pending));
                  if (read <= 0) {
                     break;
                  }
                  out.write(buff, 0, read);
                  pending -= read;
               }
            }
            out.flush();
            out.close();
            if (data != null) {
               data.close();
            }
         } catch (IOException ioe) {
            // Couldn't write? No can do.
            try {
               socket.close();
            } catch (Throwable t) {
               logger.error("Error while closing socket", t);
            }
         }
      }

   }

   static SimpleDateFormat createGmtDateFormat() {
      SimpleDateFormat gmtFormat;
      gmtFormat = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
      gmtFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      return gmtFormat;
   }

   /**
    * URL-encodes everything between "/"-characters.
    * Encodes spaces as '%20' instead of '+'.
    */
   @SuppressWarnings("UnusedDeclaration")
   private String encodeUri(String uri) throws UnsupportedEncodingException {
      String newUri = "";
      StringTokenizer st = new StringTokenizer(uri, "/ ", true);
      while (st.hasMoreTokens()) {
         String tok = st.nextToken();
         if (tok.equals("/")) {
            newUri += "/";
         } else if (tok.equals(" ")) {
            newUri += "%20";
         } else {
            newUri += URLEncoder.encode(tok, "UTF-8");
            // For Java 1.4 you'll want to use this instead:
            // try { newUri += URLEncoder.encode( tok, "UTF-8" ); } catch ( java.io.UnsupportedEncodingException uee ) {}
         }
      }
      return newUri;
   }

   private final ServerSocket serverSocket;
   private Thread connectionAcceptThread;

   private AbstractFile myRootDir;
   // ==================================================
   // File server code

   // ==================================================


   public void setMyRootDir(AbstractFile myRootDir) {
      this.myRootDir = myRootDir;
   }

   /**
    * Serves file from homeDir and its' subdirectories (only).
    * Uses only URI, ignores all headers and HTTP parameters.
    */
   public Response serveFile(
         String uri, Properties header, AbstractFile homeDir,
         boolean allowDirectoryListing) throws IOException {
      Response res = null;

      // Make sure we won't die of an exception later
      if (!homeDir.isDirectory()) {
         res = new Response(
               HTTP_INTERNALERROR, MIME_PLAINTEXT,
               "INTERNAL ERRROR: serveFile(): given homeDir is not a directory.");
      }

      if (res == null) {
         // Remove URL arguments
         uri = uri.trim().replace(File.separatorChar, '/');
         if (uri.indexOf('?') >= 0) {
            uri = uri.substring(0, uri.indexOf('?'));
         }

         // Prohibit getting out of current directory
         if (uri.startsWith("..") || uri.endsWith("..") || uri.contains("../")) {
            res = new Response(
                  HTTP_FORBIDDEN, MIME_PLAINTEXT,
                  "FORBIDDEN: Won't serve ../ for security reasons.");
         }
      }
      String userAgent = header.get("user-agent").toString();
      String rendererKey = HtmlRenderer.DESKTOP;
      if (userAgent.toLowerCase().contains("mobile")) {
         rendererKey = HtmlRenderer.MOBILE;
      }

      boolean foundInCache = false;
      InputStream cached = MemoryCache.getCachedFile(uri);
      if (cached.available() > 0) {
         foundInCache = true;
         res = new Response(HTTP_PARTIALCONTENT, MIME_DEFAULT_BINARY, cached);
         res.addHeader("Content-Length", "" + cached.available());

      }

      AbstractFile f = PathResolver.resolvePath(uri);

      if (res == null && !f.exists() && !foundInCache) {
         res = new Response(
               HTTP_NOTFOUND, MIME_PLAINTEXT,
               "Error 404, file not found.");
      }

      // List the directory, if necessary
      if (res == null && f.isDirectory()) {
         // Browsers get confused without '/' after the
         // directory, send a redirect.
         if (!uri.endsWith("/")) {
            uri += "/";
            res = new Response(
                  HTTP_REDIRECT, MIME_HTML,
                  "<html><body>Redirected: <a href=\"" + uri + "\">" +
                        uri + "</a></body></html>");
            res.addHeader("Location", uri);
         }

         if (res == null) {
            if (allowDirectoryListing && f.canRead()) {
               List<AbstractFile> files = f.listFiles();
               String html = htmlRendererMap.get(rendererKey).getRootHtml(uri, files);

               res = new Response(HTTP_OK, MIME_HTML, html);
            } else {
               res = new Response(
                     HTTP_FORBIDDEN, MIME_PLAINTEXT,
                     "FORBIDDEN: No directory listing.");
            }
         }
      }

      try {
         if (res == null) {
            long fileLen = f.length();
//            // Get MIME type from file name extension, if possible
            String mime = null;
            int dot = f.getName().lastIndexOf('.');
            if (dot >= 0) {
               mime = theMimeTypes.get(f.getName().substring(dot + 1).toLowerCase());
            }
            if (mime == null) {
               mime = MIME_DEFAULT_BINARY;
            }


            // Calculate etag
            String etag = Integer.toHexString((uri + "/" + f.getName() + "" + f.length()).hashCode());

            // Support (simple) skipping:
            long startFrom = 0;
            long endAt = -1;
            String range = header.getProperty("range");
            if (range != null) {
               if (range.startsWith("bytes=")) {
                  range = range.substring("bytes=".length());
                  int minus = range.indexOf('-');
                  try {
                     if (minus > 0) {
                        startFrom = Long.parseLong(range.substring(0, minus));
                        endAt = Long.parseLong(range.substring(minus + 1));
                     }
                  } catch (NumberFormatException ignored) {
                  }
               }
            }

            // Change return code and add Content-Range header when skipping is requested
            if (range != null && startFrom >= 0) {
               if (startFrom >= fileLen) {
                  res = new Response(HTTP_RANGE_NOT_SATISFIABLE, MIME_PLAINTEXT, "");
                  res.addHeader("Content-Range", "bytes 0-0/" + fileLen);
                  res.addHeader("ETag", etag);
               } else {
                  if (endAt < 0) {
                     endAt = fileLen - 1;
                  }
                  long newLen = endAt - startFrom + 1;
                  if (newLen < 0) {
                     newLen = 0;
                  }

                  final long dataLen = newLen;
                  InputStream fis = f.openNewStream((int) dataLen);

                  //noinspection ResultOfMethodCallIgnored
                  fis.skip(startFrom);

                  res = new Response(HTTP_PARTIALCONTENT, MIME_DEFAULT_BINARY, fis);
                  res.addHeader("Content-Length", "" + dataLen);
                  res.addHeader("Content-Range", "bytes " + startFrom + "-" + endAt + "/" + fileLen);
                  res.addHeader("ETag", etag);
               }
            } else {
               if (etag.equals(header.getProperty("if-none-match"))) {
                  res = new Response(HTTP_NOTMODIFIED, mime, "");
               } else {
                  res = new Response(HTTP_OK, mime, f.openNewStream());
                  res.addHeader("Content-Length", "" + fileLen);
                  res.addHeader("ETag", etag);
               }
            }
         }
      } catch (IOException ioe) {
         res = new Response(HTTP_FORBIDDEN, MIME_PLAINTEXT, "FORBIDDEN: Reading file failed.");
      }

      res.addHeader("Accept-Ranges", "bytes"); // Announce that the file server accepts partial content requestes
      return res;
   }

   static {
      StringTokenizer st = new StringTokenizer(
            "css		text/css " +
                  "htm		text/html " +
                  "html		text/html " +
                  "xml		text/xml " +
                  "txt		text/plain " +
                  "asc		text/plain " +
                  "gif		image/gif " +
                  "jpg		image/jpeg " +
                  "jpeg		image/jpeg " +
                  "png		image/png " +
                  "mp3		audio/mpeg " +
                  "m3u		audio/mpeg-url " +
                  "avi		video/x-msvideo " +
                  "mp4		video/mp4 " +
                  "ogv		video/ogg " +
                  "flv		video/x-flv " +
                  "mov		video/quicktime " +
                  "swf		application/x-shockwave-flash " +
                  "js			application/javascript " +
                  "pdf		application/pdf " +
                  "doc		application/msword " +
                  "ogg		application/x-ogg " +
                  "zip		application/octet-stream " +
                  "exe		application/octet-stream " +
                  "class		application/octet-stream ");
      while (st.hasMoreTokens()) {
         theMimeTypes.put(st.nextToken(), st.nextToken());
      }
   }

   public static void registerRenderer(String key, HtmlRenderer renderer) {
      htmlRendererMap.put(key, renderer);
   }

   /**
    * The distribution licence
    */
   @SuppressWarnings("UnusedDeclaration")
   private static final String LICENCE =
         "Copyright (C) 2001,2005-2011 by Jarno Elonen <elonen@iki.fi>\n" +
               "and Copyright (C) 2010 by Konstantinos Togias <info@ktogias.gr>\n" +
               "\n" +
               "Redistribution and use in source and binary forms, with or without\n" +
               "modification, are permitted provided that the following conditions\n" +
               "are met:\n" +
               "\n" +
               "Redistributions of source code must retain the above copyright notice,\n" +
               "this list of conditions and the following disclaimer. Redistributions in\n" +
               "binary form must reproduce the above copyright notice, this list of\n" +
               "conditions and the following disclaimer in the documentation and/or other\n" +
               "materials provided with the distribution. The name of the author may not\n" +
               "be used to endorse or promote products derived from this software without\n" +
               "specific prior written permission. \n" +
               " \n" +
               "THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR\n" +
               "IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES\n" +
               "OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.\n" +
               "IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,\n" +
               "INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT\n" +
               "NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,\n" +
               "DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY\n" +
               "THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n" +
               "(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE\n" +
               "OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.";
}


