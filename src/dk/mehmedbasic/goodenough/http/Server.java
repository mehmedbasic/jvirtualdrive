package dk.mehmedbasic.goodenough.http;

import dk.mehmedbasic.goodenough.AbstractFile;
import dk.mehmedbasic.goodenough.Constants;
import dk.mehmedbasic.goodenough.MemoryCache;
import dk.mehmedbasic.goodenough.Timeout;
import dk.mehmedbasic.goodenough.directorylistings.ApacheHtmlRenderer;
import dk.mehmedbasic.goodenough.directorylistings.HtmlRenderer;
import dk.mehmedbasic.goodenough.directorylistings.MobileListingStrategy;
import org.apache.log4j.*;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The main server class.

 * @author Jesenko Mehmedbasic
 * created 03-07-12, 00:37
 */
public class Server {

   public static void main(String[] args) throws Exception {
      configureLogger();


      if (args.length == 0) {
         File home = new File(Constants.CONFIG_ROOT);
         if (!home.exists()) {
            //noinspection ResultOfMethodCallIgnored
            home.mkdirs();

            writeDefaultFiles();

         }
      } else {
         if (args[0].equals("--help")) {
            printHelp();
            System.exit(-1);
         } else {
            Constants.CONFIG_ROOT = args[0];
            if (args.length == 2) {
               try {
                  Constants.PORT = Integer.parseInt(args[1]);
               } catch (NumberFormatException e) {
                  printHelp();
                  System.exit(-2);
               }
            }
         }
      }


      MemoryCache.insertFile(Constants.CONFIG_ROOT + "/mobile.css", "/mobile.css");

      AbstractFile file = Constants.resolveRoot();
      final PicoHTTPD server = new PicoHTTPD(file);
      PicoHTTPD.registerRenderer(HtmlRenderer.DESKTOP, new ApacheHtmlRenderer());
      PicoHTTPD.registerRenderer(HtmlRenderer.MOBILE, new MobileListingStrategy());

      Timer timer = new Timer(false);
      timer.schedule(
            new TimerTask() {
               @Override
               public void run() {
                  try {
                     if (Timeout.timeSinceLastRequest() > Constants.TIMEOUT) {
                        AbstractFile dir = Constants.resolveRoot();
                        server.setMyRootDir(dir);
                     }
                  } catch (Exception e) {
                     Logger.getLogger(Server.class).warn("Exception occurred.", e);
                  }

               }
            }, 0, 60 * 1000);
   }

   private static void writeDefaultFiles() throws FileNotFoundException {
      FileOutputStream stream;

      stream = new FileOutputStream(new File(Constants.CONFIG_ROOT + "/definition.xml"));
      PrintWriter writer;
      writer = new PrintWriter(stream);
      writer.print(
            "<goodenough>\n" +
                  "    <dir name=\"Good Enough Server\" >\n" +
                  "        <children>\n" +
                  "            <dir name=\"a virtual folder (.goodenougserver in user.home)\">\n" +
                  "                <sources>\n" +
                  "                    <dir path=\"" + Constants.CONFIG_ROOT + "\"/>\n" +
                  "                </sources>\n" +
                  "            </dir>\n" +
                  "        </children>\n" +
                  "    </dir>\n" +
                  "</goodenough>");
      writer.close();

      stream = new FileOutputStream(new File(Constants.CONFIG_ROOT + "/mobile.css"));
      writer = new PrintWriter(stream);
      writer.print(
            "body {\n" +
                  "    width: 320px;\n" +
                  "    color: black;\n" +
                  "}\n" +
                  "a {\n" +
                  "    color: #121212;\n" +
                  "    text-decoration: none;\n" +
                  "}\n" +
                  "\n" +
                  ".directory {\n" +
                  "    background: #ffffed;\n" +
                  "}\n" +
                  "\n" +
                  ".file {\n" +
                  "    background: #ffedff;\n" +
                  "}\n" +
                  "\n" +
                  ".movie {\n" +
                  "    background: #edffed;\n" +
                  "}\n");
      writer.close();
   }

   private static void printHelp() {
      System.out.println("JVirtualDrive 1.0 (c) 2013 Jesenko Mehmedbasic");
      System.out.println("----------------------------------------------");
      System.out.println("Usage: java -jar jvirtualdrive.jar <CONFIG_ROOT> <PORT>");
      System.out.println("<CONFIG_ROOT> : The folder containing the definition.xml file, mobile.css");
      System.out.println("                Defaults to user.home");
      System.out.println("<PORT>        : The port the server listens to");
      System.out.println("                Defaults to 1337");
      System.out.println();
      System.out.println("If values are not specified, defaults are assumed.");
      System.out.println("For more info see https://bitbucket.org/mehmedbasic/jvirtualdrive");
   }

   private static void configureLogger() throws IOException {
      PatternLayout patternLayout = new PatternLayout("%-5p [%-20.20t] (%c{1}) %m %x %n");
      String logfile = System.getenv("jvirtualdrive.log");
      if (logfile == null) {
         logfile = System.getProperty("user.home") + "/jvirtualdrive.log";
      }
      Appender appender = new DailyRollingFileAppender(patternLayout, logfile, "yyyy-MM-dd");

      Logger.getRootLogger().setLevel(Level.INFO);
      BasicConfigurator.configure(appender);
   }
}
