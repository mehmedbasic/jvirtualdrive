package dk.mehmedbasic.goodenough;

import dk.mehmedbasic.goodenough.http.PicoHTTPD;

import java.util.HashMap;
import java.util.List;

/**
 * This class resolves paths throughout the server.

 * @author Jesenko Mehmedbasic
 * created 17-08-12, 23:23
 */
public class PathResolver {
   public static final HashMap<String, AbstractFile> fileMap = new HashMap<String, AbstractFile>(4096, 1);
   private static final VirtualFile FILE_404 = new VirtualFile("");

   static {
      FILE_404.setExists(false);
   }

   public static void registerFile(String path, AbstractFile file) {
      fileMap.put(path, file);
   }

   public static AbstractFile resolvePath(String path) {
      List<String> pathFromUri = PicoHTTPD.getPathFromUri(path);
      if (pathFromUri.size() <= 1) {
         path = "/";
      } else {
         AbstractFile root = fileMap.get(pathFromUri.get(0));
         return resolvePath(root, pathFromUri, 1);
      }

      AbstractFile root = fileMap.get(path);
      if (root == null) {
         return FILE_404;
      }
      return root;
   }

   private static AbstractFile resolvePath(AbstractFile root, List<String> path, int index) {
      if (index >= path.size()) {
         return FILE_404;
      }
      String query = path.get(index);
      List<AbstractFile> list = root.listFiles();
      for (AbstractFile child : list) {
         if (child.getName().equals(query)) {
            if (index < path.size() - 1) {
               return resolvePath(child, path, index + 1);
            }
            return child;
         }
      }
      return FILE_404;
   }

}
