package dk.mehmedbasic.goodenough;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO[jekm] - someone remind me to document this class. 

 * @author Jesenko Mehmedbasic
 * created 16-08-12, 19:35
 */
public final class MemoryCache {
   private static final Map<String, byte[]> files = new HashMap<String, byte[]>();
   private static final Map<String, String> fileExtension = new HashMap<String, String>();

   public static void init() throws Exception {
      registerIcon("icons/back.png", "");
      registerIcon("icons/blank.png", "");
      registerIcon("icons/folder.png", "");
      registerIcon("icons/unknown.png", "");
      registerIcon("icons/movie.png", "avi", "mkv", "mp4", "m4v");
   }

   private static void registerIcon(String name, String... extensions) throws IOException {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      InputStream inputStream = MemoryCache.class.getResourceAsStream(name);
      int read;
      while ((read = inputStream.read()) != -1) {
         outputStream.write(read);
      }
      inputStream.close();
      String key = "/" + name;
      for (String extension : extensions) {
         fileExtension.put(extension, key);
      }
      files.put(key, outputStream.toByteArray());
   }

   public static void insertFile(String filename, String url) throws IOException {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      InputStream inputStream = new FileInputStream(filename);
      int read;
      while ((read = inputStream.read()) != -1) {
         outputStream.write(read);
      }
      inputStream.close();
      files.put(url, outputStream.toByteArray());
   }

   public static InputStream getCachedFile(String url) {
      byte[] byteData = files.get(url);
      if (byteData == null) {
         byteData = new byte[0];
      }
      return new ByteArrayInputStream(byteData);
   }

   public static String getIconDirFromFile(AbstractFile file) {
      String name = file.getName();
      int i = name.lastIndexOf(".");
      if (i != -1) {
         String extension = name.substring(i + 1).toLowerCase();
         String url = fileExtension.get(extension);
         if (url != null) {
            return url;
         }
      }
      return "/icons/unknown.png";
   }
}
