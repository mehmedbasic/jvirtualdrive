package dk.mehmedbasic.goodenough;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A wrapper for java files.

 * @author Jesenko Mehmedbasic
 * created 18-08-12, 15:42
 */
public class JavaFile implements AbstractFile {
   private File wrapped;

   public JavaFile(File wrapped) {
      this.wrapped = wrapped;
   }


   public List<AbstractFile> listFiles() {
      ArrayList<AbstractFile> abstractFiles = new ArrayList<AbstractFile>();
      File[] files = wrapped.listFiles();
      if (files != null) {
         for (File file : files) {
            abstractFiles.add(new JavaFile(file));
         }
      }
      return abstractFiles;
   }

   public boolean canRead() {
      return wrapped.canRead();
   }

   public boolean isDirectory() {
      return wrapped.isDirectory();
   }

   public long length() {
      return wrapped.length();
   }

   public String getName() {
      return wrapped.getName();
   }

   public boolean exists() {
      return wrapped.exists();
   }

   public InputStream openNewStream(final int dataLen) throws FileNotFoundException {
      return new FileInputStream(wrapped) {
         @Override
         public int available() throws IOException {
            return dataLen;
         }
      };
   }

   public InputStream openNewStream() throws FileNotFoundException {
      return new FileInputStream(wrapped);
   }

   public int compareTo(AbstractFile o) {
      return getName().compareTo(o.getName());
   }
}
