package dk.mehmedbasic.goodenough.directorylistings;

import dk.mehmedbasic.goodenough.AbstractFile;
import dk.mehmedbasic.goodenough.Constants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * TODO[jekm] - someone remind me to document this class. 

 * @author Jesenko Mehmedbasic
 * created 19-08-12, 22:08
 */
public class MobileListingStrategy implements HtmlRenderer {
   private static final String HTML_TEMPLATE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n" +
         "<html>\n" +
         "<head>\n" +
         "    <title>Index of %PWD%</title>\n" +
         "<link type=\"text/css\" rel=\"stylesheet\" href=\"/mobile.css\">" +
         "<meta name=\"viewport\" content=\"width=320;\" />" +
         "</head>\n" +
         "<body>\n" +
         "<h1>Index of %PWD%</h1>\n" +
         "%DIRLIST%" +
         "<address>" + Constants.versionString() + "</address>\n" +
         "</body>\n" +
         "</html>\n";

   public String getRootHtml(String urlDirectory, List<AbstractFile> files) throws UnsupportedEncodingException {
      String htmlTemplate = HTML_TEMPLATE.replace("%PWD%", urlDirectory);
      htmlTemplate = htmlTemplate.replace("%PWD%", urlDirectory);

      return htmlTemplate.replace("%DIRLIST%", buildDirListing(files));
   }

   private String buildDirListing(List<AbstractFile> files) throws UnsupportedEncodingException {
      StringBuilder builder = new StringBuilder();
      builder.append("<table style=\"width:320px\">");

      for (int i = 0, filesSize = files.size(); i < filesSize; i++) {
         AbstractFile file = files.get(i);
         String rowClass = "file";
         if (file.isDirectory()) {
            rowClass = "directory";
         }
         String filename = file.getName();
         if (filename.endsWith(".avi") || filename.endsWith(".mkv")) {
            rowClass = "movie";
         }

         builder.append("<tr class=\"");
         builder.append(rowClass).append("\">");

         builder.append("<td>");
         builder.append("<a href=\"").append(URLEncoder.encode(filename, "utf-8"));
         builder.append(isDir(file)).append("\">").append(filename).append(isDir(file)).append("</a>");
         builder.append("</td>");

         builder.append("</tr>");
      }
      builder.append("</table>");
      return builder.toString();
   }

   private static String isDir(AbstractFile file) {
      if (file.isDirectory()) {
         return "/";
      }
      return "";
   }

}
