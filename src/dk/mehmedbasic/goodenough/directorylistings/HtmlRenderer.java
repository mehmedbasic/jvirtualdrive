package dk.mehmedbasic.goodenough.directorylistings;

import dk.mehmedbasic.goodenough.AbstractFile;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Lists a directory

 * @author Jesenko Mehmedbasic
 * created 16-08-12, 19:13
 */
public interface HtmlRenderer {
   public static final String MOBILE = "HtmlRenderer.Mobile";
   public static final String DESKTOP = "HtmlRenderer.Desktop";

   /**
    * Lists a directory as HTML.
    *
    *
    * @param urlDirectory the directory requested in the URL.
    * @param files        the list of files in the corresponding filesystem directory.
    *
    * @return the directory listing as a String.
    *
    * @throws UnsupportedEncodingException on encoding errors.
    */
   public String getRootHtml(String urlDirectory, List<AbstractFile> files) throws UnsupportedEncodingException;
}
