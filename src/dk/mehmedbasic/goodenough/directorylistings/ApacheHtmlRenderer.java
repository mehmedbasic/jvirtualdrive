package dk.mehmedbasic.goodenough.directorylistings;

import dk.mehmedbasic.goodenough.AbstractFile;
import dk.mehmedbasic.goodenough.Constants;
import dk.mehmedbasic.goodenough.MemoryCache;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * TODO[jekm] - someone remind me to document this class. 

 * @author Jesenko Mehmedbasic
 * created 16-08-12, 02:32
 */
public class ApacheHtmlRenderer implements HtmlRenderer {
   private static final String HTML_TEMPLATE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n" +
         "<html>\n" +
         "<head>\n" +
         "    <title>Index of %PWD%</title>\n" +
         "</head>\n" +
         "<body>\n" +
         "<h1>Index of %PWD%</h1>\n" +
         "<table>\n" +
         "<tr>\n" +
         "    <th><img src=\"/icons/blank.png\" alt=\"[ICO]\"></th>\n" +
         "    <th><a href=\"?C=N;O=D\">Name</a></th>\n" +
         "    <th><a href=\"?C=M;O=A\">Last modified</a></th>\n" +
         "    <th><a href=\"?C=S;O=A\">Size</a></th>\n" +
         "    <th><a href=\"?C=D;O=A\">Description</a></th>\n" +
         "</tr>\n" +
         "<tr>\n" +
         "    <th colspan=\"5\">\n" +
         "        <hr>\n" +
         "    </th>\n" +
         "</tr>\n" +
         "<tr>\n" +
         "    <td valign=\"top\"><img src=\"/icons/back.png\" alt=\"[DIR]\"></td>\n" +
         "    <td><a href=\"%PARENT%\">Parent Directory</a></td>\n" +
         "    <td>&nbsp;</td>\n" +
         "    <td align=\"right\"> -</td>\n" +
         "</tr>\n" +
         "%DIRLIST%" +
         "\n" +
         "<tr>\n" +
         "    <th colspan=\"5\">\n" +
         "        <hr>\n" +
         "    </th>\n" +
         "</tr>\n" +
         "</table>\n" +
         "<address>" + Constants.versionString() + "</address>\n" +
         "</body>\n" +
         "</html>\n";


   public String getRootHtml(
         String cwd,
         List<AbstractFile> files) throws UnsupportedEncodingException {
      StringBuilder builder = new StringBuilder();
      if (files != null) {
         for (AbstractFile file : files) {
            builder.append("\n");
            builder.append(getHtmlForFile(file));
            builder.append("\n");
         }
      }

      String replacement = builder.toString();
      String s = HTML_TEMPLATE.replace("%PWD%", cwd);
      s = s.replace("%PWD%", cwd);
      s = s.replace("%PARENT%", cwd + "../");
      return s.replace("%DIRLIST%", replacement);
   }

   private static String getHtmlForFile(AbstractFile file) throws UnsupportedEncodingException {
      String filename = file.getName();
      String dir = "DIR";
      if (!file.isDirectory()) {
         dir = "   ";
      }
      String iconPath;
      if (file.isDirectory()) {
         iconPath = "/icons/folder.png";
      } else {
         iconPath = MemoryCache.getIconDirFromFile(file);
      }
      return "<tr>\n" +
            "    <td valign=\"top\"><img src=\"" + iconPath + "\" alt=\"[" + dir + "]\"></td>\n" +
            "    <td><a href=\"" + URLEncoder.encode(
            filename,
            "utf-8") + isDir(file) + "\">" + filename + isDir(file) + "</a></td>\n" +
            "    <td align=\"right\"> -</td>\n" +
            "    <td align=\"right\"> " + fileSize(file) + "</td>\n" +
            "</tr>";
   }

   private static String isDir(AbstractFile file) {
      if (file.isDirectory()) {
         return "/";
      }
      return "";
   }

   private static String fileSize(AbstractFile file) {
      if (file.isDirectory()) {
         return "-";
      }
      return file.length() + "";
   }
}
