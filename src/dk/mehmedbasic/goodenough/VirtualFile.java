package dk.mehmedbasic.goodenough;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class corresponding to a virtual file.

 * @author Jesenko Mehmedbasic
 * created 17-08-12, 22:19
 */
public class VirtualFile implements AbstractFile {
   private VirtualFile parent;
   private List<AbstractFile> children = new ArrayList<AbstractFile>();
   private List<AbstractFile> sources = new ArrayList<AbstractFile>();

   private boolean directory;
   private String name;
   private boolean exists = true;


   private String virtualRoot;

   public VirtualFile(String virtualRoot) {
      this.virtualRoot = virtualRoot;
   }

   public void setParent(VirtualFile parent) {
      this.parent = parent;
   }

   private List<AbstractFile> getChildren() {
      return children;
   }

   public List<AbstractFile> getSources() {
      return sources;
   }

   public List<AbstractFile> listFiles() {
      ArrayList<AbstractFile> list = new ArrayList<AbstractFile>();
      for (AbstractFile source : getSources()) {
         list.addAll(source.listFiles());
      }
      list.addAll(children);
      Collections.sort(list);
      return list;
   }

   public boolean isDirectory() {
      return directory;
   }

   public long length() {
      return 42;
   }


   public void setDirectory(boolean directory) {
      this.directory = directory;
   }


   public String getName() {
      return name;
   }

   public boolean exists() {
      return exists;
   }

   public boolean canRead() {
      return true;
   }

   public InputStream openNewStream() throws FileNotFoundException {
      throw new UnsupportedOperationException("Virtual file has no streams");
   }

   public InputStream openNewStream(final int available) throws FileNotFoundException {
      throw new UnsupportedOperationException("Virtual file has no streams");
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return "ImaginaryFile{" +
            "name='" + name + '\'' +
            ", parent=" + (parent == null ? null : parent.getName()) +
            '}';
   }

   public void setExists(boolean exists) {
      this.exists = exists;
   }

   public String getRoot() {
      if (virtualRoot == null) {
         virtualRoot = "";
      }
      return virtualRoot;
   }

   public void addChild(AbstractFile imaginaryFile) {
      getChildren().add(imaginaryFile);
      PathResolver.registerFile(getRoot(), imaginaryFile);
   }

   public int compareTo(AbstractFile o) {
      return getName().compareTo(o.getName());
   }
}
