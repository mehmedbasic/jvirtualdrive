package dk.mehmedbasic.goodenough;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * TODO[jekm] - someone remind me to document this class. 

 * @author Jesenko Mehmedbasic
 * created 18-08-12, 15:41
 */
public interface AbstractFile extends Comparable<AbstractFile> {

   public List<AbstractFile> listFiles();

   public boolean canRead();

   public boolean isDirectory();

   public long length();

   public String getName();

   boolean exists();

   InputStream openNewStream(int dataLen) throws FileNotFoundException;

   InputStream openNewStream() throws FileNotFoundException;
}
