package dk.mehmedbasic.goodenough.serialization;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * The XML version of the directory.

 * @author Jesenko Mehmedbasic
 * created 17-08-12, 21:35
 */
@Root(name = "dir")
public class VirtualDir {
   @Attribute(name = "path", required = false)
   private String path;

   @Attribute(name = "name", required = false)
   private String name;

   @ElementList(required = false)
   private List<VirtualDir> sources = new ArrayList<VirtualDir>();

   @ElementList(required = false)
   private List<VirtualDir> children = new ArrayList<VirtualDir>();


   public VirtualDir() {
   }

   public String getName() {
      return name;
   }

   public String getPath() {
      return path;
   }

   public List<VirtualDir> getSources() {
      return sources;
   }

   public List<VirtualDir> getChildren() {
      return children;
   }

   public boolean isVirtual() {
      //noinspection SimplifiableIfStatement
      if (children.size() != 0 || sources.size() != 0) {
         return true;
      }
      return path == null;
   }
}
