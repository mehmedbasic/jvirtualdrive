package dk.mehmedbasic.goodenough.serialization;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * A root definition.

 * @author Jesenko Mehmedbasic
 * created 17-08-12, 21:42
 */
@Root(name = "goodenough")
public class RootDefinition {
   @Attribute(required = false)
   private String server;

   @Element(name = "dir")
   private VirtualDir root;

   public RootDefinition() {
   }

   public VirtualDir getRoot() {
      return root;
   }

   public String getServer() {
      return server;
   }
}
